# Router CMS

`date : ~spring-summer 2022`

This project had the goal to give access to the only people that could connect to the interface.

The router running on a raspberry pi had to protect the private network and so,  hosted this website to restrict internet access to connected users.

## Prerequesties

This app does not require to have a running postgres DB as everything was in the database folder but requires the modules from the requirements.txt :
```
flask
psycopg2
```

Also need **postgres** installed

## Run

You only need to run
```bash
chmod +x starter.sh && ./starter.sh
```
The `chmod` is here to ensure that you have de right to execute (`+x`) the file.

## Screenshots

V2 login screen
![Login screen on V2](screenshots/cms_v2_login.png)

V1 login screen
![Login screen on V1](screenshots/cms_v1_login.png)

I tried different design systems, the one from github and the one from hackthebox.
I remember beeing in a trafic jam in mountains in bosnia so I worked on it.