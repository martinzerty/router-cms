from datetime import timedelta
from flask import Flask, render_template, request, flash, redirect, session
from blueprints import admin
from workers import db

app = Flask(__name__)
app.config['SECRET_KEY'] = 'lol'
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(minutes=10)
app.register_blueprint(admin.admin)

d = db.Manager()

@app.route('/')
def index():
    if not 'user' in session:
        return redirect('/home')
    return redirect('/admin')

@app.route('/home')
def home():
    if 'user' in session:
        return redirect('/admin')
    return render_template('home.html')

@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        form = request.form
        if d.check_user('ADM', request.form):
            session['user'] = True
            flash('Bienvenue !','success')
            return redirect('/')
        flash('Mauvais identifiants :/', 'error')
        return redirect('/login')
    return render_template('login.html')

@app.route('/logout')
def logout():
    if 'user' in session:
        session.clear()
        msg = "Merci de vous être déconnecté"
    else:
        msg = "Vous n'est pas connecté"
    flash(msg, 'success')
    return redirect('/login')

if __name__ == '__main__':
    app.run(debug=True, port=8080)