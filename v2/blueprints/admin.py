from flask import Blueprint, render_template, session, make_response, redirect

admin = Blueprint('admin', url_prefix='/admin', import_name='admin')

@admin.route('/')
def adm_index():
    if not 'user' in session:
        return redirect('/')
    return render_template('admin/index.html', server_name='azerty-relation 1')

@admin.route('/messages')
def messages():
    return render_template('admin/messages.html')