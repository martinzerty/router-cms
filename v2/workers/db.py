import psycopg2
import hashlib

class Manager:
    def __init__(self) -> None:
        self.con = psycopg2.connect(dbname='router', host='localhost', port='5432', user='router_cms', password='RsZq40w1!')
        self.cur = self.con.cursor()
    def check_user(self, cat, creds):
        """
        --cat : 'ADM' or 'USR',
        --creds : {'username':str, 'password': str}
        """
        self.cur.execute(f"SELECT * FROM credentials WHERE serial_number LIKE '{cat}%' AND username='{creds['username']}'")
        rows = self.cur.fetchone()
        if rows:
            if len(rows) > 0:
                hash = hashlib.sha256()
                hash.update(creds['password'].encode('utf-8'))
                hash = hash.hexdigest()
                print(hash, rows[3])
                if hash == rows[3]:
                    return True
        return False