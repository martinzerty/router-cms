from flask import Flask, render_template, request, flash, redirect
import requests

app = Flask(__name__)
app.config['SECRET_KEY'] = 'lol'

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        form = request.form
        if form['username'] == 'martin' and form['password'] == 'martin':
            flash('Bienvenue !','success')
            return render_template('admin.html')
        flash('Mauvais identifiants :/', 'error')
        return redirect('/')
    return redirect('/')

if __name__ == '__main__':
    app.run(debug=True, port=8080)