#!/bin/bash
set -a
source .env
source colors.sh

echo -e  "${BYellow}[Launching] : Clearing ports${Color_Off}"
echo -e "${Red}"
kill -9 $(lsof -t -i:${BW_PORT})
kill -9 $(lsof -t -i:${FLASK_PORT})
kill -9 $(lsof -t -i:${POSTGRES_PORT})
echo -e "${Color_Off}"

echo -e "${BYellow}[Launching] : If flask is running, it means the api is running too.${Color_Off}"
# bw serve --port 9090 --hostname localhost &
postgres -D database &  flask run -h $FLASK_HOST -p $FLASK_PORT # & open 
"http://127.0.0.1:8080"
